%--------------------------------------------------------------------------
% matrix_Zbr_calc_s.m - function to calculate akustic matrix of bassreflex
% tube in laplace form
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function A = matrix_Zbr_calc_s(r, l, Zbr)
    rho = density_of_air();
    s = tf('s');
    
    Abr = r^2*pi;
    Za_1 = s * rho*l/Abr;
    Za = Za_1 + Zbr;
    
    a11 = 1;
    a12 = 0;
    a21 = 1/Za;
    a22 = 1;    
    A = [a11 a12; a21 a22];
end
