%--------------------------------------------------------------------------
% mmguS_calc_inv.m - function to calculate fuS and QmMk from mmguS, smMa
% and RmMa. This function is to be called when mmguS changes.
% Multiple parameters are adapted to ensure the mechanical parameters and 
% Bl stay unchanged.
%
% This file is part of Speaker Analyzer.
% 
% Copyright 2020 Markus Faymann
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function [fuS, QmMk, QeMk] = mmguS_calc_inv(mmguS, smMa, RmMa, Rs, Bl)

% Equation 2.19 in Sch�ch2020 with mguS being mmguS here
fuS = 1/(2*pi)*sqrt(smMa/mmguS);

% Equation 3.67 in Sch�ch2018
QmMk = sqrt(smMa*mmguS)/RmMa;

% Equation 3.68 in Sch�ch2018 
QeMk = Rs / Bl^2 * sqrt(smMa * mmguS);
end