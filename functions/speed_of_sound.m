function c = speed_of_sound()
%SPEED_OF_SOUND Get the sound velocity in air.

    c = 343.2;      % sound velocity in air [m/s] at 20�C
    %c = 345;        % Used by Klippel
end

