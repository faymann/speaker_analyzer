%--------------------------------------------------------------------------
% imp_function.m - function to calculate impedance function
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function y = imp_function(w, TuS, QmMk, RmguS, Bl, Rs)
    s = 1i.*w;
    ReguS = Bl^2 / RmguS;
    y = Rs + ReguS/QmMk * s.*TuS./(1+s.*TuS/QmMk+s.^2*TuS^2);
end