%--------------------------------------------------------------------------
% matrix_Rl_calc.m - function to calculate akustic matrix of leakage looses 
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function A = matrix_Rl_calc(alpha, smMa, Ql, Th, Am)
    savG = alpha*smMa/Am^2;
    Ra = real(Za_calc(Am, 1/Th));
    RaVvG = Ql * Th*savG - Ra;
    
    a11 = 1;
    a12 = 0;
    a21 = 1/RaVvG;
    a22 = 1;    
    A = [a11 a12; a21 a22];
end
