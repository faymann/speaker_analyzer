function [C] = C_calc(creep, w)
%ZC_CALC Calculate the (possibly complex-valued) suspension compliance for 
% the specified frequency using the specified creep model and its 
% parameters.
%
% Input parameters:
% arg ... A struct which may contain the following fields.
%         type (required)
%         sls.c0 (required if type is 'SLS')
%         sls.c1 (required if type is 'SLS')
%         sls.n1 (required if type is 'SLS')
%         exp.c0 (required if type is 'Exp')
%         exp.beta (required if type is 'Exp')
%         log.c0 (required if type is 'SimpleLog' or 'ComplexLog')
%         log.lambda (required if type is 'SimpleLog' or 'ComplexLog')
%         ritter.c0 (required if type is 'Ritter3' or 'Ritter4')
%         ritter.kappa (required if type is 'Ritter3' or 'Ritter4')
%         ritter.tmin (required if type is 'Ritter3' or 'Ritter4')
%         ritter.tmax (required if type is 'Ritter4')
% w ..... The angular frequency.
%
% Output parameters:
% C ... The suspension compliance at the frequency w.
%
    if strcmp(creep.type, 'SLS')
        C = creep.sls.c0 + 1 / (1/creep.sls.c1 + 1i * w * creep.sls.n1);
    elseif strcmp(creep.type, 'Exp')
        C = creep.exp.c0 * (1i * w)^(-creep.exp.beta);
    elseif strcmp(creep.type, 'SimpleLog')
        C = creep.log.c0 * (1 - creep.log.lambda * log10(w));
    elseif strcmp(creep.type, 'ComplexLog')
        C = creep.log.c0 * (1 - creep.log.lambda * log10(1i*w));
    elseif strcmp(creep.type, 'Ritter3')
        tmin = creep.ritter.tmin;
        C = creep.ritter.c0 * (1 - creep.ritter.kappa * log10((1i * w*tmin * exp(-1i * atan(w*tmin))/sqrt(1 + (w*tmin)^2))));  
    elseif strcmp(creep.type, 'Ritter4')
        tmin = creep.ritter.tmin;
        tmax = creep.ritter.tmax;
        phi = atan(w * (tmax - tmin) / (1 + tmax * tmin * w^2));
        C = creep.ritter.c0 * (1 - creep.ritter.kappa * log10(sqrt((tmin^2 + tmin^2 * tmax^2 * w^2)/(tmax^2 + tmin^2 * tmax^2 * w^2))) * exp(1i * phi));    
    else
        error('Unknown suspension creep model.');
    end
end

