%-----------------------------------------------------------------------------
% x_function_vG.m - function to calculate displacement function bassreflex box
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%-----------------------------------------------------------------------------

function y = x_function_vG(w, Ug, Rg, Rs, Bl, mmguS, Th, TuS, QgvG, Ql, alpha)
    s = 1i.*w;
    y = Ug/(Rg+Rs) * Bl*TuS^2/mmguS * (s.^2*Th^2 + s.*Th/Ql + 1) ./...
        (s.^4*Th^2*TuS^2 + s.^3*(Th^2*TuS/QgvG+Th*TuS^2/Ql) + s.^2*(Th^2*(1+alpha)+Th*TuS/(Ql*QgvG)+TuS^2) + s.*(Th/Ql+TuS/QgvG) +1);
end