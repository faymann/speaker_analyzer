%--------------------------------------------------------------------------
% p_matrix_model_calc.m - Calculate the sound pressure at a specified
% distance and angle from the membrane using the model of a konphas
% oscillating area.
%
% Input parameters:
% q ....... The velocity of the membrane in m/s.
% r ....... The distance to the center of the membrane in meters.
% theta ... The angle between the rotation axis of the membrane and the
%           point of interest.
% Am ...... The area of the membrane in m�.
% w ....... The angular frequencies that are to be evaluated.
%
% Output paramter:
% p ... The sound pressure at the specified location (distance and angle).
%
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function p = p_matrix_model_calc(q, r, theta, Am, w)
    c = speed_of_sound();
    rho = density_of_air();
    
    theta_rad = theta*pi/180;
    rm = sqrt(Am/pi);
    k = w/c;

    if (theta_rad == 0)
        gamma_ko = 1;
    else
        gamma_ko = 2.*besselj(1,k.*rm.*sin(theta_rad))./(k.*rm.*sin(theta_rad));
    end
    
    p = q.*gamma_ko.*1i.*k.*rho.*c .* exp(-1i.*k.*r)./(4*r*pi);
end
