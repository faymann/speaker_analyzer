%--------------------------------------------------------------------------
% matrix_electr_calc_s.m - function to calculate electrical matrix laplace 
% from Rs, Ls
%
% Input parameters:
% Rs  ... The voice coil DV resistance in ?
% Zle ... A struct which specifies the model for the voice coil inductance
%         and its model parameters.
%         .type ... the model ('L', 'L2R', 'L3R' or 'L2RK')
%         Further properties depending on the type: Le, L2, R2, L3, R3, Ke
%
% Output parameter:
% A ... the chain matrix
%
% This file is part of Speaker Analyzer.
% 
% Copyright 2020 Markus Faymann
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function A = matrix_electr_calc_s(Rs, Zle)
    s = tf('s');
    Z_Le = s*Zle.Le;
    Z_L2 = s*Zle.L2;
    Z_L3 = s*Zle.L3;  
    if strcmp(Zle.type, 'L')
        Z = Rs + Z_Le;
    elseif strcmp(Zle.type, 'L2R')
        if Zle.L2 == 0 || Zle.R2 == 0
           Z = Rs + Z_Le + 0;
        else
           Z = Rs + Z_Le + 1/(1/Z_L2 + 1/Zle.R2);
        end  
    elseif strcmp(Zle.type, 'L3R')        
        if Zle.L2 == 0 || Zle.R2 == 0
           Z2 = 0;
        else
           Z2 = 1/(1/Z_L2 + 1/Zle.R2);
        end 
        if Zle.L3 == 0 || Zle.R3 == 0
           Z3 = 0;
        else
           Z3 = 1/(1/Z_L3 + 1/Zle.R3);
        end
        Z = Rs + Z_Le + Z2 + Z3;
    elseif strcmp(Zle.type, 'L2RK')
        error('Cannot calculate transfer function for L2RK model. No valid transfer function in the s-domain exists.');
        %Z_K = Zle.Ke * (1+1i) * sqrt(1i/2) * s^(1/2);
        %Z = Rs + Z_Le + 1/(1/Z_K + 1/Z_L2 + 1/Zle.R2);   
    else
        error('Unsupported model for Z_LE.');
    end
    
    a11 = 1;
    a12 = Z;
    a21 = 0;
    a22 = 1;

    A = [a11 a12; a21 a22];
end
