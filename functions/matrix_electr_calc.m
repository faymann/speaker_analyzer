%--------------------------------------------------------------------------
% matrix_electr_calc.m - function to calculate electrical matrix from Rs, 
% Ls, w
%
% Input parameters:
% Rs  ... The voice coil DV resistance in ?
% ZLe ... A struct which specifies the model for the voice coil inductance
%         and its model parameters.
%         .type ... the model ('L', 'L2R', 'L3R', 'L2RK', 'Leach' or 
%                   'Wright')
%         Further properties depending on the type: Le, L2, R2, L3, R3, K2
% w   ... The angular frequency.
%
% Output parameter:
% A ... the chain matrix
%
% This file is part of Speaker Analyzer.
% 
% Copyright 2020 Markus Faymann
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function A = matrix_electr_calc(Rs, ZLe, w)    
    if strcmp(ZLe.type, 'L')
        Z_Le = 1i*w*ZLe.Le; 
        Z = Rs + Z_Le;
    elseif strcmp(ZLe.type, 'L2R')   
        Z_Le = 1i*w*ZLe.Le; 
        Z_L2 = 1i*w*ZLe.L2;
        Z = Rs + Z_Le + 1/(1/Z_L2 + 1/ZLe.R2);
    elseif strcmp(ZLe.type, 'L3R')    
        Z_Le = 1i*w*ZLe.Le; 
        Z_L2 = 1i*w*ZLe.L2;
        Z_L3 = 1i*w*ZLe.L3; 
        Z = Rs + Z_Le + 1/(1/Z_L2 + 1/ZLe.R2) + 1/(1/Z_L3 + 1/ZLe.R3);
    elseif strcmp(ZLe.type, 'L2RK')   
        Z_Le = 1i*w*ZLe.Le; 
        Z_L2 = 1i*w*ZLe.L2;
        Z_K = ZLe.K2 * (1+1i) * sqrt(w/2);
        Z = Rs + Z_Le + 1/(1/Z_K + 1/Z_L2 + 1/ZLe.R2);     
    elseif strcmp(ZLe.type, 'Leach')
        Z = Rs + ZLe.K * (1i * w)^(ZLe.n);
    elseif strcmp(ZLe.type, 'Wright')
        Z = Rs + ZLe.Kr * w^ZLe.Er + 1i * ZLe.Kx * w^ZLe.Ex;
    else
        error('Unsupported model for Z_LE.');
    end
    
    a11 = 1;
    a12 = Z;
    a21 = 0;
    a22 = 1;

    A = [a11 a12; a21 a22];
end
