function rho = density_of_air()
%DENSITY_OF_AIR Get the density of air in kg/m�
    
    rho = 1.2041;   % density of air 1,2041kg/m^3 at 20�C
    %rho = 1.189;    % Used by Klippel
end

