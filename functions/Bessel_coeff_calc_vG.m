%----------------------------------------------------------------------------------------------
% Bessel_coeff_calc_vG.m - function to calculate bessel characteristic coefficients a1, a2, a3
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%----------------------------------------------------------------------------------------------

function [a1,a2,a3] = Bessel_coeff_calc_vG()

a1 = 3.20108;
a2 = 4.39155;
a3 = 3.12394;

end