%--------------------------------------------------------------------------
% matrix_electromech_calc.m - function to calculate electromechanical 
% transducer matrix from Bl
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function A = matrix_electromech_calc(Bl)
    a11 = Bl;
    a12 = 0;
    a21 = 0;
    a22 = 1/Bl;
    A = [a11 a12; a21 a22];
end
