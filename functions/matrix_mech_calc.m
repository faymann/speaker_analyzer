%--------------------------------------------------------------------------
% matrix_mech_calc.m - function to calculate mechanical matrix from Rm_Ma, 
% sm_Ma, m_Mk, w
%
% Input parameters:
% Rm_Ma ... The mechanical friction resistance of the membrane.
% sm_Ma ... The mechanical stiffness of the membrane. Only used if creep 
%           model type is 'None'.
% m_Mk  ... The mass of the membrane.
% creep ... A struct with the parameters for the suspension creep model.
%           See function C_calc for more details.
% w     ... The angular frequency.
%
% Output parameter:
% A ... the chain matrix
%
% This file is part of Speaker Analyzer.
% 
% Copyright 2020 Markus Faymann
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function A = matrix_mech_calc(Rm_Ma, sm_Ma, m_Mk, creep, w)
    if strcmp(creep.type, 'None')
        Cms = 1/sm_Ma;      
    else
        Cms = C_calc(creep, w);
    end

    a11 = 1;
    a12 = 0;
    a21 = Rm_Ma + 1/(1i*w*Cms) + 1i*w*m_Mk;
    a22 = 1;
    A = [a11 a12; a21 a22];
end
