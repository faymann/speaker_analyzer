# Speaker Analyzer

Speaker Analyzer is an open source Matlab program to analyse and dimension loudspeaker systems. Loudspeaker systems with closed and bassreflex enclosures and the model of a speaker in the infinite baffle are supported. The sound pressure level of the loudspeakers can be calculated directly from the Thiele-Small parameters of a speaker and relates to the sound characteristics of the loudspeaker system. With the specification of the sound characteristics the parameters of the enclosures were calculated automatically. Therefore the program is the ideal tool to optimize a loudspeaker in all aspects to the desired sound characteristics.

The program is based on the bachelor thesis [Elektroakustische Modellbildung und
Optimierung von Lautsprechersystemen](https://www.spsc.tugraz.at/student-projects/elektroakustische-modellbildung-und-optimierung-von-lautsprechersystemen.html) and has been improved as part of the master projects [Realisierung eines modularen Lautsprechersystems](https://www.spsc.tugraz.at/student-projects/realisierung-eines-modularen-lautsprechersystems.html) and [Electro-mechanical modelling of dynamic loudspeakers](https://www.spsc.tugraz.at/student-projects/erweiterte-elektroakustische-modelle.html). All works have been conducted at the Graz University of Technology.

## Getting Started

* Start Matlab on your system
* Open Speaker_Analyzer.m
* Run Speaker_Analyzer

How to use the program can be found [here](https://www2.spsc.tugraz.at/www-archive/downloads/Elektroakustische_Modellbildung_und_Optimierung_von_Lautsprechersystemen_final.pdf#page=77). (german only)

### Prerequisites

* Matlab R2015a or newer

## License

This project is licensed under the GNU General Public License - see the [LICENSE](LICENSE) file for details

## Author

* Markus Faymann
* Florian Loacker-Schöch
