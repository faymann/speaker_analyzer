# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

### Added
- Add support for advanced models for the voice coil. However, those are only available when using the *Extended Model* setting.
- Add support for models that take into account the suspension creep. However, those are only available when using the *Extended Model* setting.
- Add support for exporting curves to a MAT-file.
- Add support for exporting the transfer function Hx(ω) = x(ω)/Ug which is exported when exporting the displacement of the membrane.
- Add support for displaying and changing the Bl factor.
- Add function `matrix_model_calc_fA` which calculates the impedance of the loudspeaker and the velocity of the membrane in free air.
- Add application version number to the window title.

### Changed
- Improve the performance by using less frequency points (1000 instead of 20000) but with an improved spacing (logarithmic instead of linear).
- Translate the graphical user interface to English.
- Allow changing the mechanical parameters more easily. You can now change a mechanical parameter without it affecting other mechanical parameters. 
- Remove the square brackets from the units.

## [2.1] - 2020-08-03
## [2.0] - 2019-11-27
## [1.2] - 2018-11-01

